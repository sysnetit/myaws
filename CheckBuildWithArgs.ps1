﻿# Pass in args
param 
( 
        [Parameter(Mandatory=$True)]
        [string]$domain
)

# Log file to check
$file = Get-Content "C:\Windows\Debug\postbuild.log"



# Input parameters
#$domain = "EUROPE"
$timezone = "GMT Standard Time"
$site = "London"
$cpus = 1
$memory = 1
$os = "Microsoft Windows Server 2012 R2 Standard"
$disk = "c:"
$disksize = "30GB"
$services = @("Print Spooler","Server","Task Sxxcheduler","Plug and Play","Virtual Disk")
$user = "takako"

# Hashtable of strings to check exist
$checkstrings = @{
domaintext = "SERVICE: Domain selection was DEFAULT, Domain is $domain"
sitetext = "SERVICE-DEPLOY-TMPLTPOSTBLD: AD Site has been determined to be $site"
missingtext = "hdfkjsdhfkjsdhf"
timezonetext = "SERVICE-CONFIGURE: Time Zone set as $timezone"
}

# Hashtable of results
$results = @{}

# Loop through hashtable checking if in file. Add result to results hashtable
Foreach ($key in $checkstrings.keys)
    {
    $value = $checkstrings[$key]
    if ([bool]($file -like "*$value*"))
        { 
        write-debug "$value"
        $results.add($key, "TRUE")
        }
    else 
        {
        write-host -ForegroundColor Red "Cant find : $value"
        $results.add($key, "FALSE")
        }
    }




# Check memory
$wmiComp = Get-WmiObject -Class Win32_ComputerSystem 
$actualmemory = (([System.Math]::Round($wmiComp.TotalPhysicalMemory/1GB,2)).ToString() + "GB") 

If ($actualmemory -eq $memory)
    {
    $results.add("memory", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red "Actual memory = $actualmemory"
    $results.add("memory", "FALSE")
    }

# Check CPU
$actualcpus = $wmiComp.NumberOfProcessors 
If ($actualcpus -eq $cpus)
    {
    $results.add("cpus", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red "Actual CPUs = $actualcpus"
    $results.add("cpus", "FALSE")
    }

# Check O/S
$wmiComp = Get-WmiObject -Class Win32_OperatingSystem 
$actualos = $wmiComp.Caption 
If ($actualos -eq $os)
    {
    $results.add("os", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red $wmiComp.Caption
    $results.add("os", "FALSE")
    }

# Check extra disk
$wmiDisk = Get-WmiObject -Class Win32_LogicalDisk | where{$_.Caption -eq "$disk"}
$actualsize = (([System.Math]::Round($wmiDisk.size/1GB)).ToString() + "GB") 
If ($actualsize -eq $disksize)
    {
    $results.add("disk", "TRUE")
    }
else
    {
    Write-host -ForegroundColor Red $actualsize
    $results.add("disk", "FALSE")
    }

# Check activated
$licensekey = (Get-WmiObject -Class SoftwareLicensingProduct| Where {$_.PartialProductKey})
[string]$license = $licensekey.licensestatus
If ($license -eq '1') 
    {
    $results.add("activated", "TRUE")
    }
else
    {
    write-host -ForegroundColor Red "activated"
    $results.add("activated", "FALSE")
    }
    
# Check services
Foreach ($service in $services)
    {
    try
        {
        $servicedetail = get-service $service -ErrorAction Stop
        If ($servicedetail.status -eq "running" -and $servicedetail.StartType -eq "auto") 
            {
            $results.add($service, "TRUE")
            }
        else
            {
            write-host -ForegroundColor Red "$service   $($servicedetail.status)    $($servicedetail.StartType)"
            $results.add($service, "FALSE")
            }
        }
    catch
        {
        write-host -ForegroundColor Red "Can't find : $service"
        $results.add($service, "FALSE")
        }    
    }  

# Check user in Remote Desktop Users
$UserExists = [bool](Get-WmiObject -Class Win32_GroupUser | Where-Object {$_.GroupComponent -match "Remote Desktop Users" -and
$_.PartComponent.Contains($user)})
If ($UserExists)
    {
    $results.add("user", "TRUE")
    }
else
    {
    $results.add("user", "FALSE")
    }



$results 

$results | out-string | set-content c:\windows\debug\postbuild-check.txt




#Reverse Lookup
#Services
#-	Autosys
#-	Sccm
#-	NoSS
#-	NSEM
#-	Sentinel
#-	OfficeScan
#-	Greyware
#-	Netbackup



