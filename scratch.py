#Get list of buckets in AWS account
#Ask for input and show files in bucket.


# Get list of buckets
import boto3
import sys
import time

s3 = boto3.resource('s3')


# Check if input is integer and within range
def validated(userinput):

    try:
        val = int(userinput)
        if val > 0 and val <= counter:
            valid = True
        else:
            valid = False

    except Exception:
        valid = False

    return valid

# Print out bucket number and name
while True:

    bucketlist=[]
    counter=0
    for bucket in s3.buckets.all():

        counter += 1
        print(counter,bucket.name)
        bucketlist.append(bucket.name)

# Check which bucket to use
    bucketnumber = input ("\nWhich bucket do you want to check ?  (q=stop)")
    if bucketnumber == 'q':
        print('\nFinishing\n')

        break
    if validated(bucketnumber):
        bucketnumber=int(bucketnumber)-1
        bucketname = (bucketlist[bucketnumber])
        my_bucket = s3.Bucket(bucketname)

        print ('\nBucket name: {}\n'.format(bucketname))
        try:
            for s3_file in my_bucket.objects.all():
                print(s3_file.key)
        except:
            print ('\nCan\'t find anything\n')
            time.sleep(2)
    else:
        print('\nInvalid input - please try again\n')
        time.sleep(2)

