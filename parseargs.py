import argparse
import getpass

description_txt = "Hello {user}.\n Welcome to my script".format(user=getpass.getuser())
epilog_txt = "please ensure....."

try:
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description=description_txt,epilog=epilog_txt)
    parser.add_argument("-u", "--username", help="username", action='store', required=True)
    parser.add_argument("-r", "--region", help="region", action='store', required=True)
    cmdline_args = vars(parser.parse_args())
    user1 = cmdline_args["username"]
    region1 = cmdline_args["region"]
    print(user1, region1)
except:
    print("error")
